# Seasons
This project is part of a series of React projects. The goal of this is to practice and enhance my React knowledge. Some of this projects will use APIs that I'm going to make with Django (Python), Express & Node (Javascript) and Golang.

---

## Getting Started
If you want to clone this repo and run it in your machine, this is what you need to do:
- Place yourself in the project root folder
- Run the following command in order to install the packages:
```
npm i
```
- Now you can run the project with the following command:
```
npm start
```

Please, let me know if you have any trouble.

---

## Visit the project here

https://zego.gitlab.io/seasons
