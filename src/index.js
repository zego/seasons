// React
import React from 'react';
import ReactDOM from 'react-dom';

// Components
import SeasonDisplay from './SeasonDisplay';
import Spinner from './Spinner';

// const App = () => {
//     window.navigator.geolocation.getCurrentPosition(
//         position => console.log(position),
//         err => console.log(err)
//     );

//     return (
//         <h1>Hola</h1>
//     );
// };

class App extends React.Component {

    // constructor(props) {
    //     super(props);
    //     console.log('Calling constructor method');
    //     // THIS IS THE ONLY TIME we do direct assigment to this.state
    //     this.state = {
    //         lattitude: null,
    //         errorMesage: ''
    //     };
    // }

    state = {
        lattitude: null,
        errorMessage: ''
    }

    componentDidMount() {
        window.navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({ lattitude: position.coords.latitude });
            },
            (err) => {
                this.setState({ errorMesage: err.message });
            }
        );
    }

    componentDidUpdate() {
        console.log('Calling componentDidUpdate method');
    }

    renderContent() {
        if (this.state.errorMesage && !this.state.lattitude){
            return (
                <div>Error: {this.state.errorMesage}</div>
            )
        }
        if (!this.state.errorMesage && this.state.lattitude) {
            return (
                <SeasonDisplay
                    lattitude={this.state.lattitude}
                />
            )
        }
        return (
            <Spinner
                message="Please accept location request"
            />
        )
    }

    render () {
        return (
            <div>
                {this.renderContent()}
            </div>
        )
    }
}

ReactDOM.render(
    <App />,
    document.querySelector('#root')
);
